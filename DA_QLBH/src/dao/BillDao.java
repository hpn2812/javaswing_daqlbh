/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entities.BillEntity;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ADMIN
 */
public class BillDao {

    public static List<BillEntity> getListBill() {
        List<BillEntity> list = new ArrayList<>();
        ConnectDB cn = new ConnectDB();
        String sql = "select * from hoadon";
        try {
            ResultSet rs = cn.executeQuery(sql);
            while (rs.next()) {
                BillEntity bill = new BillEntity();
                bill.setId(rs.getString(1));
                bill.setDate(rs.getDate(2).toLocalDate());
                bill.setNameOfMember(rs.getString(3));
                bill.setQuantity(Integer.parseInt(rs.getString(4)));
                bill.setPrice(Float.parseFloat(rs.getString(5)));
                bill.setTotalMoney(Float.parseFloat(rs.getString(6)));
                bill.setId_Product(rs.getString(7));
                bill.setId_Employee(rs.getString(8));
                list.add(bill);
            }
            cn.Close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static List<BillEntity> getPrice(String s) {
        List<BillEntity> DSloai = new ArrayList<>();
        ConnectDB cn = new ConnectDB();
        String sql = "select gia from sanpham where masp='" + s + "'";
        try {
            ResultSet rs = cn.executeQuery(sql);
            while (rs.next()) {
                BillEntity loai = new BillEntity();
                loai.setPrice(Float.parseFloat(rs.getString(1)));
                DSloai.add(loai);
            }
            cn.Close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return DSloai;
    }

    public static boolean addBill(BillEntity ca, String sql) throws Exception {
        boolean kq = false;
        ConnectDB cn = new ConnectDB();
        int n = cn.executeUpdate(sql);
        if (n == 1) {
            kq = true;
        }
        cn.Close();
        return kq;
    }
    //Search by  id_Bill
    
    public static ArrayList<BillEntity> Search(String sql){
        ArrayList<BillEntity> list = new ArrayList<>();
        try {
            ConnectDB cn= new ConnectDB();
            ResultSet rs = cn.executeQuery(sql);
            while (rs.next()) {                
               BillEntity bill = new BillEntity();
                bill.setId(rs.getString(1));
                bill.setDate(rs.getDate(2).toLocalDate());
                bill.setNameOfMember(rs.getString(3));
                bill.setQuantity(Integer.parseInt(rs.getString(4)));
                bill.setPrice(Float.parseFloat(rs.getString(5)));
                bill.setTotalMoney(Float.parseFloat(rs.getString(6)));
                bill.setId_Product(rs.getString(7));
                bill.setId_Employee(rs.getString(8));
                list.add(bill);
            }
            cn.Close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
}
