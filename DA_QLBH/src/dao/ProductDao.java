/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entities.BillEntity;
import entities.CategoryEntity;
import entities.ProductEntity;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ADMIN
 */
public class ProductDao {

    public static List<ProductEntity> listProduct() {
        List<ProductEntity> list = new ArrayList<>();
        ConnectDB cn = new ConnectDB();
        try {
            String sql = "select * from sanpham";
            ResultSet rs = cn.executeQuery(sql);
            while (rs.next()) {
                ProductEntity product = new ProductEntity();
                product.setId_Product(rs.getString(1));
                product.setName_Product(rs.getString(2));
                product.setPrice(rs.getFloat(3));
                product.setDescribe(rs.getString(4));
                product.setSize(rs.getString(5));
                product.setDate_of_manufacture(rs.getDate(6).toLocalDate());
                product.setQuantity(rs.getString(7));
                product.setId_Category(rs.getString(8));
                list.add(product);
            }
            cn.Close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static boolean addProduct(ProductEntity pro, String sql) throws Exception {
        boolean kq = false;
        ConnectDB cn = new ConnectDB();
        int n = cn.executeUpdate(sql);
        if (n == 1) {
            kq = true;
        }
        cn.Close();
        return kq;
    }

    public static boolean deleteProduct(String masp) throws Exception {
        boolean kq = false;
        String sql = "delete from sanpham where masp= '" + masp + "'";
        ConnectDB cn = new ConnectDB();
        int n = cn.executeUpdate(sql);
        if (n == 1) {
            kq = true;
        }
        cn.Close();
        return kq;
    }

    public static boolean updateProduct(String tensp, float gia, String moTa, String size, LocalDate ngaysx, int soLuong, String maTl, String masp) {
        boolean kq = false;
        String sql = "update sanpham set tensp=N'" + tensp + "',gia=" + gia + ",mota=N'" + moTa + "',size='" + size + "',ngaysx='" + ngaysx + "',soluongcon=" + soLuong + ",matl='" + maTl + "' where masp='" + masp + "'";
        ConnectDB cn = new ConnectDB();
        int n = cn.executeUpdate(sql);
        if (n == 1) {
            kq = true;
        }
        try {
            cn.Close();
        } catch (Exception ex) {
            Logger.getLogger(CategoryDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return kq;
    }
    
    //Search by id_product
      public static ArrayList<ProductEntity> Search(String sql){
        ArrayList<ProductEntity> list = new ArrayList<>();
        try {
            ConnectDB cn= new ConnectDB();
            ResultSet rs = cn.executeQuery(sql);
            while (rs.next()) {                
              ProductEntity product = new ProductEntity();
                product.setId_Product(rs.getString(1));
                product.setName_Product(rs.getString(2));
                product.setPrice(rs.getFloat(3));
                product.setDescribe(rs.getString(4));
                product.setSize(rs.getString(5));
                product.setDate_of_manufacture(rs.getDate(6).toLocalDate());
                product.setQuantity(rs.getString(7));
                product.setId_Category(rs.getString(8));
                list.add(product);
            }
            cn.Close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

   
}
