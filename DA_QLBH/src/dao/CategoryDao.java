/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entities.CategoryEntity;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ADMIN
 */
public class CategoryDao {

    public static List<CategoryEntity> getListCategory() {
        List<CategoryEntity> DSloai = new ArrayList<>();
        ConnectDB cn = new ConnectDB();
        String sql = "select * from theloai ";
        try {
            ResultSet rs = cn.executeQuery(sql);
            while (rs.next()) {
                CategoryEntity loai = new CategoryEntity();
                loai.setId_Category(rs.getString(1));
                loai.setName_Category(rs.getString(2));
                DSloai.add(loai);
            }
            cn.Close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return DSloai;
    }
    //Thêm mới the loại
    public static boolean addCategory(CategoryEntity ca, String sql) throws Exception {
        boolean kq = false;
        ConnectDB cn = new ConnectDB();
        int n = cn.executeUpdate(sql);
        if (n == 1) {
            kq = true;
        }
        cn.Close();
        return kq;
    }
    
    //xóa thể loại
     public static boolean deleteCategory(String matl) throws Exception
   {
       boolean kq=false;
       String sql="delete from theloai where matl='"+matl+"'";
       ConnectDB cn= new ConnectDB();
       int n=cn.executeUpdate(sql);
       if(n==1)
       {
           kq=true;
       }
       cn.Close();
       return kq;
   }
     
     //Sửa  the loai
      public static boolean suadg(String tendg,String matl) {
        boolean kq = false;
        String sql = "update theloai set tentl=N'" + tendg +"'where matl='"+matl+"'";
        ConnectDB cn= new ConnectDB();
        int n = cn.executeUpdate(sql);
        if (n == 1) {
            kq = true;
        }
        try {
            cn.Close();
        } catch (Exception ex) {
            Logger.getLogger(CategoryDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return kq;
    }
      
      //search by id_Category
       public static ArrayList<CategoryEntity> Search(String sql){
        ArrayList<CategoryEntity> list = new ArrayList<>();
        try {
            ConnectDB cn= new ConnectDB();
            ResultSet rs = cn.executeQuery(sql);
            while (rs.next()) {                
             CategoryEntity loai = new CategoryEntity();
                loai.setId_Category(rs.getString(1));
                loai.setName_Category(rs.getString(2));
                list.add(loai);
            }
            cn.Close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
}
