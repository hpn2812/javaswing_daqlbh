/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author ADMIN
 */
public class CategoryEntity {
    private  String id_Category;
    private  String name_Category;

    public CategoryEntity() {
    }

    public CategoryEntity(String id_Category, String name_Category) {
        this.id_Category = id_Category;
        this.name_Category = name_Category;
    }
    

    public String getId_Category() {
        return id_Category;
    }

    public void setId_Category(String id_Category) {
        this.id_Category = id_Category;
    }

    public String getName_Category() {
        return name_Category;
    }

    public void setName_Category(String name_Category) {
        this.name_Category = name_Category;
    }

    
}
