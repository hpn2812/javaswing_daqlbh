/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.time.LocalDate;
import java.util.Date;

/**
 *
 * @author ADMIN
 */
public class ProductEntity {
    private  String id_Product;
    private  String name_Product;
    private  float price;
    private  String describe;
    private  String size;
    private  LocalDate date_of_manufacture; // ngay sx
    private  String quantity;
    private  String id_Category;

    public ProductEntity() {
    }

    public String getId_Product() {
        return id_Product;
    }

    public void setId_Product(String id_Product) {
        this.id_Product = id_Product;
    }

    public String getName_Product() {
        return name_Product;
    }

    public void setName_Product(String name_Product) {
        this.name_Product = name_Product;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public LocalDate getDate_of_manufacture() {
        return date_of_manufacture;
    }

    public void setDate_of_manufacture(LocalDate date_of_manufacture) {
        this.date_of_manufacture = date_of_manufacture;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getId_Category() {
        return id_Category;
    }

    public void setId_Category(String id_Category) {
        this.id_Category = id_Category;
    }

    public ProductEntity(String id_Product, String name_Product, float price, String describe, String size, LocalDate date_of_manufacture, String quantity, String id_Category) {
        this.id_Product = id_Product;
        this.name_Product = name_Product;
        this.price = price;
        this.describe = describe;
        this.size = size;
        this.date_of_manufacture = date_of_manufacture;
        this.quantity = quantity;
        this.id_Category = id_Category;
    }


    
}
